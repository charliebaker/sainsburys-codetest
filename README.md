# Sainsburys Code Test - Charlie Baker

Here are some notes regarding the code test completed.

## Issues
  - The Sainsbury's website required a long "Cookie" header for the request to properly get the HTML otherwise it would not return the data, instead asking me to turn on Javascript. I used Charles Proxy and nearly used the Ajax call directly instead, it had the same issue. I then rehosted the HTML on my own website at cj-baker.co.uk/sainsburys/response.txt , it might be better to re-host a static version of the pag somewhere for future candidates as it makes the code test significantly harder.

## Libraries
- Dagger 2 - Dependency Injection framework
- RxJava and Retrolambda - Reactive extensions
- ButterKnife - View Injection Framework
- Retrofit, OkhttpLogging - Network calls and logging.
- Android Support, Design and Databinding Libraries
- Timber - Logging
- Fresco - Remote image loading

## Architecture
I used the "Clean Code" architecture of splitting the application into 3 modules: "app", "domain" and "data".
These are mostly self explanatory, the app module contains the Android application itself whilst the Domain module contains the domain/business logic; finally the Data module just handles the data side of things.

### App
The application uses the MVP (Model-View-Presenter) method. I know the document for the code test specifies MVC however I could not find much material online relating to Android and MVC specifically and the examples I found were not what I would expect to be in production code. Android doesn't seem to blend well with MVC from what I can see however I am happy to be corrected. For this reason I went down the MVP route as it seperates concerns and is what I have used in the past and has worked really well.

### Domain
This is a pure Java module that has contains the domain or business logic and use cases / interactors. This will make it easier to test and port logic to other platforms.

### Data
The data layer uses the repository pattern to obfuscate data sources which can be added/removed at will without having to change logic in the other layers of the project.

## Improvements
- I would add some Unit Tests
- Add in proper error handling, just didn't have time to finish that off.
- Error handling such as Retrying.
- Offline caching of data.
- Currency detection.