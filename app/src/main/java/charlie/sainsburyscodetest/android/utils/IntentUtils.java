package charlie.sainsburyscodetest.android.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class IntentUtils {

    public static void launchBrowser(Context c, String url) {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            c.startActivity(i);
        } catch (ActivityNotFoundException e) {
            throw new ActivityNotFoundException(e.getMessage());
        }
    }

}
