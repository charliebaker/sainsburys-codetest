package charlie.sainsburyscodetest.android;

import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;

import charlie.sainsburyscodetest.BuildConfig;
import timber.log.Timber;

public class SainsburysApplication extends Application {

    private SainsburysComponent mSainsburysComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);

        initializeLogging();
        buildComponentAndInject();
    }

    private void initializeLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void buildComponentAndInject() {
        mSainsburysComponent = SainsburysComponent.Initializer.init(this);
        mSainsburysComponent.inject(this);
    }

    public SainsburysComponent component() {
        return mSainsburysComponent;
    }

    public static SainsburysApplication get(Context context) {
        return (SainsburysApplication) context.getApplicationContext();
    }

}
