package charlie.sainsburyscodetest.android.ui.products;

import javax.inject.Inject;

import charlie.sainsburyscodetest.android.base.mvp.BasePresenter;
import charlie.sainsburyscodetest.domain.interactor.GetProductsInteractor;
import charlie.sainsburyscodetest.domain.model.ProductLine;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class ProductsPresenterImpl extends BasePresenter<ProductsView> implements ProductsPresenter {

    private GetProductsInteractor mGetProductsInteractor;

    @Inject
    public ProductsPresenterImpl(GetProductsInteractor getProductsInteractor) {
        mGetProductsInteractor = getProductsInteractor;
    }

    @Override
    public void onLoaded() {
        if (isViewAttached()) {
            getView().hideLoadingError();
            getView().hideContent();
            getView().showLoading();

            startFetchProducts();
        }
    }

    private void startFetchProducts() {
        mGetProductsInteractor.getProductLists()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onProductsFetched, this::onError);
    }

    private void onError(Throwable throwable) {
        Timber.e(throwable, "Error!! ");
        // We should do some proper error handling here such as showing a retry button.
    }

    private void onProductsFetched(ProductLine productLine) {
        if (isViewAttached()) {
            getView().setProducts(productLine.getProductList());
            getView().setProductListTotal(productLine.getTotalCost());

            getView().hideLoading();
            getView().showContent();
        }
    }
}
