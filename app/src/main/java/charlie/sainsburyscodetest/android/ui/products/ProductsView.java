package charlie.sainsburyscodetest.android.ui.products;


import java.math.BigDecimal;
import java.util.List;

import charlie.sainsburyscodetest.android.base.mvp.BaseView;
import charlie.sainsburyscodetest.domain.model.Product;

public interface ProductsView extends BaseView {

    void showLoading();

    void hideLoading();

    void showLoadingError();

    void hideLoadingError();

    void setProducts(List<Product> products);

    void setProductListTotal(BigDecimal total);

    void showContent();

    void hideContent();

}
