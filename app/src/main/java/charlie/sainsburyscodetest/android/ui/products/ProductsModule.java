package charlie.sainsburyscodetest.android.ui.products;

import charlie.sainsburyscodetest.domain.interactor.GetProductsInteractor;
import charlie.sainsburyscodetest.domain.interactor.GetProductsInteractorImpl;
import dagger.Module;
import dagger.Provides;

@Module
public class ProductsModule {

    @Provides
    @ProductsScope
    ProductsPresenter providePresenter(GetProductsInteractor getProductsInteractor) {
        return new ProductsPresenterImpl(getProductsInteractor);
    }

    @Provides
    @ProductsScope
    GetProductsInteractor provideProductsInteractor(GetProductsInteractorImpl getProductsInteractor) {
        return getProductsInteractor;
    }

}
