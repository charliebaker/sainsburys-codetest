package charlie.sainsburyscodetest.android.ui.products;

import charlie.sainsburyscodetest.android.SainsburysComponent;
import dagger.Component;

@ProductsScope
@Component(
    dependencies = SainsburysComponent.class,
    modules = ProductsModule.class
)
public interface ProductsComponent {
    void inject(ProductsActivity activity);
}
