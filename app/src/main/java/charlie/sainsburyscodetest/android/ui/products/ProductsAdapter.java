package charlie.sainsburyscodetest.android.ui.products;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import charlie.sainsburyscodetest.R;
import charlie.sainsburyscodetest.android.utils.IntentUtils;
import charlie.sainsburyscodetest.domain.model.Product;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private static final NumberFormat NUMBER_FORMATTER = NumberFormat.getCurrencyInstance(Locale.UK);
    private List<Product> mProductList;

    public ProductsAdapter(List<Product> productList) {
        mProductList = productList;
    }

    @Override
    public ProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_product, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ProductsAdapter.ViewHolder holder, int position) {
        holder.bindProduct(mProductList.get(position));
    }

    @Override
    public int getItemCount() {
        return mProductList != null ? mProductList.size() : 0;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.list_item_product_image)
        SimpleDraweeView mProductImage;

        @BindView(R.id.list_item_product_title)
        TextView mProductTitle;

        @BindView(R.id.list_item_product_price_per_unit)
        TextView mProductPricePerUnit;

        @BindView(R.id.list_item_product_price_per_measure)
        TextView mProductPricePerMeasure;

        private String mProductUrl;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindProduct(Product product) {
            mProductUrl = product.getUrl();
            mProductImage.setImageURI(Uri.parse(product.getImageUrl()));

            mProductTitle.setText(product.getName());
            mProductPricePerUnit.setText(mProductImage.getContext().getString(R.string.products_price_per_unit,
                    NUMBER_FORMATTER.format(product.getPricePerUnit())));
            mProductPricePerMeasure.setText(mProductImage.getContext().getString(R.string.products_price_per_measure,
                    NUMBER_FORMATTER.format(product.getPricePerMeasure())));
        }

        @OnClick(R.id.list_item_product_container)
        void onContainerClick() {
            IntentUtils.launchBrowser(mProductImage.getContext(), mProductUrl);
        }

    }

}
