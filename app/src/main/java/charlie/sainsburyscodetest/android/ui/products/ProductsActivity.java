package charlie.sainsburyscodetest.android.ui.products;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import charlie.sainsburyscodetest.R;
import charlie.sainsburyscodetest.android.SainsburysComponent;
import charlie.sainsburyscodetest.android.base.mvp.BaseActivity;
import charlie.sainsburyscodetest.databinding.ActivityProductsBinding;
import charlie.sainsburyscodetest.domain.model.Product;

public class ProductsActivity extends BaseActivity<ProductsPresenterImpl> implements ProductsView {

    @Inject ProductsPresenterImpl mPresenter;
    private ProductsComponent mProductsComponent;

    private List<Product> mProductList;
    private ProductsAdapter mProductsAdapter;

    private ActivityProductsBinding mActivityProductsBinding;

    private NumberFormat mNumberFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Could probably factor this out into the BaseActivity at some point.
        mActivityProductsBinding = DataBindingUtil.setContentView(this, layoutId());
        mNumberFormat = NumberFormat.getCurrencyInstance(Locale.UK);
    }

    @Override
    protected int layoutId() {
        return R.layout.activity_products;
    }

    @Override
    protected void onCreateComponent(SainsburysComponent sainsburysComponent) {
        mProductsComponent = DaggerProductsComponent.builder()
                .sainsburysComponent(sainsburysComponent)
                .build();
        mProductsComponent.inject(this);
    }

    @Override
    protected void onNewView() {
        super.onNewView();
        getPresenter().onLoaded();
    }

    @Override
    protected void onViewRestored() {
        // Can add in some behaviour to reload from cache for example here.
        super.onViewRestored();
        getPresenter().onLoaded();
    }

    @Override
    protected ProductsPresenterImpl createPresenter() {
        return mPresenter;
    }

    @Override
    public void showLoading() {
        mActivityProductsBinding.productsProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mActivityProductsBinding.productsProgress.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingError() {
        mActivityProductsBinding.productsError.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingError() {
        mActivityProductsBinding.productsError.setVisibility(View.GONE);
    }

    @Override
    public void setProducts(List<Product> products) {
        if (mProductList != null) {
            mProductList.clear();
        } else {
            mProductList = new ArrayList<>();
        }
        mProductList.addAll(products);

        if (mProductsAdapter == null) {
            mProductsAdapter = new ProductsAdapter(mProductList);
            mActivityProductsBinding.productsRecyclerview.setLayoutManager(new LinearLayoutManager(this));
            mActivityProductsBinding.productsRecyclerview.setAdapter(mProductsAdapter);
        } else {
            mProductsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void setProductListTotal(BigDecimal total) {
        mActivityProductsBinding.productsTotal.setText(getString(R.string.products_total,
                mNumberFormat.format(total.doubleValue())));
    }

    @Override
    public void showContent() {
        mActivityProductsBinding.productsContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideContent() {
        mActivityProductsBinding.productsContent.setVisibility(View.GONE);
    }
}
