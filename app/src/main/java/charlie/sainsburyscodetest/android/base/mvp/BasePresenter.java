package charlie.sainsburyscodetest.android.base.mvp;

public abstract class BasePresenter<T extends BaseView> {

    private T mView;

    protected T getView() {
        return mView;
    }

    public void bindView(T view) {
        mView = view;
    }

    public void unbindView() {
        mView = null;
    }

    protected boolean isViewAttached() {
        return getView() != null;
    }

    public void onDestroy() {
        unbindView();
    }

}
