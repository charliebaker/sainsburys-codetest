package charlie.sainsburyscodetest.android.base.mvp;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import charlie.sainsburyscodetest.android.SainsburysApplication;
import charlie.sainsburyscodetest.android.SainsburysComponent;

public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements BaseView {

    private P mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SainsburysApplication sainsburysApplication = SainsburysApplication.get(this);

        onCreateComponent(sainsburysApplication.component());
        mPresenter = createPresenter();
        if (mPresenter == null) {
            throw new IllegalStateException("Presenter was not created! Please return presenter in createPresenter()!");
        }
        getPresenter().bindView(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (savedInstanceState == null) {
            onNewView();
        } else {
            onViewRestored();
        }
    }

    protected abstract @LayoutRes int layoutId();

    protected abstract void onCreateComponent(SainsburysComponent sainsburysComponent);

    protected abstract P createPresenter();

    protected P getPresenter() {
        return mPresenter;
    }

    protected void onNewView() {

    }

    protected void onViewRestored() {

    }

    @Override
    protected void onDestroy() {
        getPresenter().onDestroy();
        mPresenter = null;
        super.onDestroy();
    }

}
