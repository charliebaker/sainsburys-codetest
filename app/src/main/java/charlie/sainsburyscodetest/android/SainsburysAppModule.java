package charlie.sainsburyscodetest.android;

import android.app.Application;
import android.content.Context;

import charlie.sainsburyscodetest.data.repository.products.ProductDataRepository;
import charlie.sainsburyscodetest.domain.repository.ProductRepository;
import charlie.sainsburyscodetest.internal.di.ApplicationScope;
import dagger.Module;
import dagger.Provides;

@Module
public class SainsburysAppModule {

    private final SainsburysApplication mApplication;

    public SainsburysAppModule(SainsburysApplication app) {
        mApplication = app;
    }

    @Provides
    @ApplicationScope
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationScope
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @ApplicationScope
    ProductRepository provideProductRepository(ProductDataRepository repository) {
        return repository;
    }
}