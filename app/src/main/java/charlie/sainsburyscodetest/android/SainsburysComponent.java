package charlie.sainsburyscodetest.android;

import android.content.Context;


import charlie.sainsburyscodetest.api.ApiModule;
import charlie.sainsburyscodetest.domain.repository.ProductRepository;
import charlie.sainsburyscodetest.internal.di.ApplicationScope;
import dagger.Component;

@ApplicationScope
@Component(modules = { SainsburysAppModule.class, ApiModule.class})
public interface SainsburysComponent {

    void inject(SainsburysApplication sainsburysApplication);

    /**
     * An initializer that creates the graph from an application.
     */
    final class Initializer {
        static SainsburysComponent init(SainsburysApplication app) {
            return DaggerSainsburysComponent.builder()
                    .sainsburysAppModule(new SainsburysAppModule(app))
                    .build();
        }

        private Initializer() {} // No instances.
    }

    /* Application related */
    Context context();

    /* Repoistiries */
    ProductRepository productRepository();

}
