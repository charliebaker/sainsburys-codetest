package charlie.sainsburyscodetest.domain.repository;

import charlie.sainsburyscodetest.domain.model.ProductLine;
import rx.Observable;

public interface ProductRepository {

    Observable<ProductLine> getProducts();

}
