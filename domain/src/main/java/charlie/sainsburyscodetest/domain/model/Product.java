package charlie.sainsburyscodetest.domain.model;

import java.math.BigDecimal;

public class Product {

    private String mName;
    private String mImageUrl;
    private String mUrl;

    private BigDecimal mPricePerUnit;
    private BigDecimal mPricePerMeasure;

    public Product() {

    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public BigDecimal getPricePerUnit() {
        return mPricePerUnit;
    }

    public void setPricePerUnit(BigDecimal pricePerUnit) {
        mPricePerUnit = pricePerUnit;
    }

    public BigDecimal getPricePerMeasure() {
        return mPricePerMeasure;
    }

    public void setPricePerMeasure(BigDecimal pricePerMeasure) {
        mPricePerMeasure = pricePerMeasure;
    }
}
