package charlie.sainsburyscodetest.domain.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ProductLine {

    private List<Product> mProductList;
    private BigDecimal mTotalCost;

    public ProductLine() {
        mProductList = new ArrayList<>();
        mTotalCost = new BigDecimal("0.00");
    }

    public List<Product> getProductList() {
        return mProductList;
    }

    public void addProduct(Product product) {
        mProductList.add(product);
        mTotalCost = mTotalCost.add(product.getPricePerUnit());
    }

    public BigDecimal getTotalCost() {
        return mTotalCost;
    }
}
