package charlie.sainsburyscodetest.domain.interactor;

import javax.inject.Inject;

import charlie.sainsburyscodetest.domain.model.ProductLine;
import charlie.sainsburyscodetest.domain.repository.ProductRepository;
import rx.Observable;

public class GetProductsInteractorImpl implements GetProductsInteractor {

    @Inject ProductRepository mProductRepository;

    @Inject
    public GetProductsInteractorImpl(ProductRepository productRepository) {
        mProductRepository = productRepository;
    }

    @Override
    public Observable<ProductLine> getProductLists() {
        return mProductRepository.getProducts();
    }

}
