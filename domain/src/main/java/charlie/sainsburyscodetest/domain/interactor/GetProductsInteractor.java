package charlie.sainsburyscodetest.domain.interactor;

import charlie.sainsburyscodetest.domain.model.ProductLine;
import rx.Observable;

public interface GetProductsInteractor {

    Observable<ProductLine> getProductLists();

}
