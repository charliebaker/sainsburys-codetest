package charlie.sainsburyscodetest.data.repository.products.datasource;

import javax.inject.Inject;

import charlie.sainsburyscodetest.data.net.SainsburysService;
import charlie.sainsburyscodetest.internal.di.ApplicationScope;

@ApplicationScope
public class ProductDataStoreFactory {

    private SainsburysService mSainsburysService;

    @Inject
    public ProductDataStoreFactory(SainsburysService sainsburysService) {
        mSainsburysService = sainsburysService;
    }

    public ProductDataStore createCloudDataStore() {
        return new ProductCloudDataStore(mSainsburysService);
    }

}
