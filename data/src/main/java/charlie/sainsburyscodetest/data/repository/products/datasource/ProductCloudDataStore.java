package charlie.sainsburyscodetest.data.repository.products.datasource;

import javax.inject.Inject;

import charlie.sainsburyscodetest.data.net.SainsburysService;
import charlie.sainsburyscodetest.internal.di.ApplicationScope;
import okhttp3.ResponseBody;
import rx.Observable;

@ApplicationScope
public class ProductCloudDataStore implements ProductDataStore {

    private SainsburysService mSainsburysService;

    @Inject
    public ProductCloudDataStore(SainsburysService sainsburysService) {
        mSainsburysService = sainsburysService;
    }

    @Override
    public Observable<ResponseBody> productLineEntityList() {
        return mSainsburysService.getProductLine();
    }
}
