package charlie.sainsburyscodetest.data.repository.products.datasource;

import okhttp3.ResponseBody;
import rx.Observable;

public interface ProductDataStore {

    Observable<ResponseBody> productLineEntityList();

}
