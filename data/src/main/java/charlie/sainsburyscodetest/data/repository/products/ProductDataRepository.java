package charlie.sainsburyscodetest.data.repository.products;

import javax.inject.Inject;

import charlie.sainsburyscodetest.data.entity.mapper.ProductEntityMapper;
import charlie.sainsburyscodetest.data.repository.products.datasource.ProductDataStoreFactory;
import charlie.sainsburyscodetest.domain.model.ProductLine;
import charlie.sainsburyscodetest.domain.repository.ProductRepository;
import charlie.sainsburyscodetest.internal.di.ApplicationScope;
import rx.Observable;

@ApplicationScope
public class ProductDataRepository implements ProductRepository {

    private final ProductDataStoreFactory mProductDataStoreFactory;
    private final ProductEntityMapper mProductEntityMapper;

    @Inject
    public ProductDataRepository(ProductDataStoreFactory productDataStoreFactory, ProductEntityMapper productEntityMapper) {
        mProductDataStoreFactory = productDataStoreFactory;
        mProductEntityMapper = productEntityMapper;
    }

    @Override
    public Observable<ProductLine> getProducts() {
        return mProductDataStoreFactory.createCloudDataStore().productLineEntityList()
                .map(var -> mProductEntityMapper.transform(var));
    }
}
