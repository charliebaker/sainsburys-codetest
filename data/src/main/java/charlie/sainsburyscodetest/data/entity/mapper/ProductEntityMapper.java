package charlie.sainsburyscodetest.data.entity.mapper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.math.BigDecimal;

import javax.inject.Inject;

import charlie.sainsburyscodetest.domain.model.Product;
import charlie.sainsburyscodetest.domain.model.ProductLine;
import charlie.sainsburyscodetest.internal.di.ApplicationScope;
import okhttp3.ResponseBody;

@ApplicationScope
public class ProductEntityMapper {

    @Inject
    public ProductEntityMapper() {

    }

    public ProductLine transform(ResponseBody productPageResponse) {
        ProductLine productLine = new ProductLine();

        try {
            String responseHtml = productPageResponse.string();
            Document doc = Jsoup.parse(responseHtml);

            Elements productDivs = doc.select("div.product");
            for (int i  = 0; i < productDivs.size(); i++) {
                Product p = transformProductDiv(productDivs.get(i));
                if (p != null) {
                    productLine.addProduct(p);
                }
            }
        } catch (IOException e) {
            return productLine;
        }
        return productLine;
    }

    private Product transformProductDiv(Element productDiv) {
        Product p = null;

        if (productDiv != null) {
            p = new Product();

            Element productLink = productDiv.select("h3 a").get(0);
            String url = productLink.attr("href");
            String title = productLink.text();
            String imgUrl = productLink.child(0).attr("src");

            Element pricingContainer = productDiv.select("div.pricing").first();
            String pricePerUnit = stripCurrency(pricingContainer.select("p.pricePerUnit").first().ownText());
            String pricePerMeasure = stripCurrency(pricingContainer.select("p.pricePerMeasure").first().ownText());

            p.setName(title);
            p.setUrl(url);
            p.setImageUrl(imgUrl);
            p.setPricePerUnit(new BigDecimal(pricePerUnit));
            p.setPricePerMeasure(new BigDecimal(pricePerMeasure));
        }

        return p;
    }

    private String stripCurrency(String number) {
        // This is obviously not the correct way to do this across multiple currencies.
        return number.substring(1, number.length());
    }

}
