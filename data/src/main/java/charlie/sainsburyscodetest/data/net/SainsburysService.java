package charlie.sainsburyscodetest.data.net;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import rx.Observable;

public interface SainsburysService {

    /**
     * Note: This is obviously not the correct way of doing this. Ideally we would use Query or Path paramters and pass in the Category ID's etc
     * into the method to return the appropriate content.
     */
    @GET("sainsburys/response.txt")
    Observable<ResponseBody> getProductLine();
}
