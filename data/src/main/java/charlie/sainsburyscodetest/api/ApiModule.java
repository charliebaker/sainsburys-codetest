package charlie.sainsburyscodetest.api;

import charlie.sainsburyscodetest.data.net.SainsburysService;
import charlie.sainsburyscodetest.internal.di.ApplicationScope;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

@Module
public class ApiModule {

    private static final String BASE_URL = "http://cj-baker.co.uk/";

    @Provides
    @ApplicationScope
    public SainsburysService provideSainsburysService(Retrofit retrofit) {
        return retrofit.create(SainsburysService.class);
    }

    @Provides
    @ApplicationScope
    public Retrofit.Builder provideRetrofitBuilder(OkHttpClient client) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client);
    }

    @Provides
    @ApplicationScope
    public OkHttpClient provideClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
        return client;
    }

    @Provides
    @ApplicationScope
    public Retrofit provideRetrofit(String baseUrl, Retrofit.Builder builder) {
        return builder.baseUrl(baseUrl).build();
    }

    @Provides
    @ApplicationScope
    public String provideBaseUrl() {
        return BASE_URL;
    }

}